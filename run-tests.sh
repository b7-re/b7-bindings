#!/bin/sh

# Top-level test suite

set -e

pwd="$(pwd)"

for lang in c python
do
    cd "$pwd" || exit 1
    cd "$lang" || exit 1
    if ! ./run-tests.sh
    then
	exit 1
    fi
done
