#include <assert.h>
#include <stdio.h>

#include <b7.h>

static const char *flag = "dr4g0n_or_p4tric1an_it5_LLVM";

static bool ui_update(void *data, B7UiResults *results, uint64_t min)
{
	printf("update: data = %p, results = %p, min = %u\n",
	       data,
	       results,
	       (unsigned int) min);

	return true;
}

static bool ui_wait(void *data)
{
	printf("wait: data = %p\n", data);

	return true;
}

static bool ui_done(void *data)
{
	printf("done: data = %p\n", data);

	return true;
}

static void ui_set_timeout(void *data, uint64_t timeout)
{
	printf("set_timeout: data = %p, timeout = %u\n",
	       data, (unsigned int) timeout);
}

static uint64_t ui_get_timeout(void *data)
{
	(void) data;
	return 5;
}

int main(void)
{
	// Create custom UI
	B7UiCustom *custom = b7_ui_custom_new(NULL, ui_update, ui_wait, ui_done,
					      ui_set_timeout, ui_get_timeout);
	B7Ui *ui = b7_ui_from_custom(custom);

	// Solve wyvern
	B7Opts *opts = b7_opts_new("../test-bins/wyvern");
	b7_opts_set_solve_stdin(opts, true);
	b7_opts_set_timeout(opts, 5);
	b7_opts_set_ui(opts, ui);
	B7Results results = b7_run(opts);

	// Assert results are sane
	assert(results.input);
	assert(!results.error);
	size_t len = b7_input_get_stdin_len(results.input);
	assert(len == 29);
	for (size_t i = 0; flag[i]; i++) {
		char c = b7_input_get_stdin_byte(results.input, i);
		assert(c == flag[i]);
	}
}
