#include <assert.h>

#include <b7.h>

static const char *flag = "dr4g0n_or_p4tric1an_it5_LLVM";

int main(void)
{
	// Solve wyvern
	B7Opts *opts = b7_opts_new("../test-bins/wyvern");
	b7_opts_set_solve_stdin(opts, true);
	b7_opts_set_timeout(opts, 5);
	B7Results results = b7_run(opts);

	// Assert results are sane
	assert(results.input);
	assert(!results.error);
	size_t len = b7_input_get_stdin_len(results.input);
	assert(len == 29);
	for (size_t i = 0; flag[i]; i++) {
		char c = b7_input_get_stdin_byte(results.input, i);
		assert(c == flag[i]);
	}
}
