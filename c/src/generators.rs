use std::ffi;
use std::os::raw::c_char;

#[derive(Clone)]
pub struct B7Input(pub b7::generators::Input);

#[derive(Clone)]
pub struct B7MemInput(pub b7::generators::MemInput);

#[no_mangle]
pub unsafe extern "C" fn b7_input_new() -> *mut B7Input {
    Box::into_raw(Box::new(B7Input(b7::generators::Input::new())))
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_set_argc(inp: *mut B7Input, argc: u32) {
    let mut inp = Box::from_raw(inp);
    inp.0.argc = Some(argc);
    Box::into_raw(inp);
}

fn opt_vec_push<T>(opt: &mut Option<Vec<T>>, item: T) {
    match opt {
        Some(vec) => vec.push(item),
        None => *opt = Some(vec![item]),
    }
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_argvlens_push(inp: *mut B7Input, argvlen: u32) {
    let mut inp = Box::from_raw(inp);
    opt_vec_push(&mut inp.0.argvlens, argvlen);
    Box::into_raw(inp);
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_argv_push(inp: *mut B7Input, arg: *const c_char) {
    let mut inp = Box::from_raw(inp);
    let arg = ffi::CStr::from_ptr(arg).to_bytes().to_vec();
    opt_vec_push(&mut inp.0.argv, arg);
    Box::into_raw(inp);
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_set_stdinlen(inp: *mut B7Input, stdinlen: u32) {
    let mut inp = Box::from_raw(inp);
    inp.0.stdinlen = Some(stdinlen);
    Box::into_raw(inp);
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_set_stdin(inp: *mut B7Input, stdin: *const u8, len: usize) {
    let mut inp = Box::from_raw(inp);
    let stdin = std::slice::from_raw_parts(stdin, len).to_vec();
    inp.0.stdin = Some(stdin);
    Box::into_raw(inp);
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_mem_push(inp: *mut B7Input, mem: *mut B7MemInput) {
    let mut inp = Box::from_raw(inp);
    let mem = Box::from_raw(mem);
    opt_vec_push(&mut inp.0.mem, mem.0.clone());
    Box::into_raw(mem);
    Box::into_raw(inp);
}

#[no_mangle]
pub unsafe extern "C" fn b7_mem_input_new() -> *mut B7MemInput {
    Box::into_raw(Box::new(B7MemInput(b7::generators::MemInput::default())))
}

#[no_mangle]
pub unsafe extern "C" fn b7_mem_input_set_size(mem: *mut B7MemInput, size: usize) {
    let mut mem = Box::from_raw(mem);
    mem.0.size = size;
    Box::into_raw(mem);
}

#[no_mangle]
pub unsafe extern "C" fn b7_mem_input_set_addr(mem: *mut B7MemInput, addr: usize) {
    let mut mem = Box::from_raw(mem);
    mem.0.addr = addr;
    Box::into_raw(mem);
}

#[no_mangle]
pub unsafe extern "C" fn b7_mem_input_set_bytes(
    mem: *mut B7MemInput,
    bytes: *const u8,
    len: usize,
) {
    let mut mem = Box::from_raw(mem);
    let bytes = std::slice::from_raw_parts(bytes, len).to_vec();
    mem.0.bytes = bytes;
    Box::into_raw(mem);
}

#[no_mangle]
pub unsafe extern "C" fn b7_mem_input_set_breakpoint(mem: *mut B7MemInput, breakpoint: usize) {
    let mut mem = Box::from_raw(mem);
    mem.0.breakpoint = Some(breakpoint);
    Box::into_raw(mem);
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_get_argc(inp: *mut B7Input) -> u32 {
    let inp = Box::from_raw(inp);
    let ret = inp.0.argc;
    Box::into_raw(inp);
    ret.unwrap_or(0)
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_get_argvlens_len(inp: *mut B7Input) -> usize {
    let inp = Box::from_raw(inp);
    let ret = inp.0.argvlens.as_ref().map(|vec| vec.len()).unwrap_or(0);
    Box::into_raw(inp);
    ret
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_get_argvlens_item(inp: *mut B7Input, index: usize) -> u32 {
    let inp = Box::from_raw(inp);
    let ret = inp.0.argvlens.as_ref().expect("argvlens is empty")[index];
    Box::into_raw(inp);
    ret
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_get_argv_len(inp: *mut B7Input) -> usize {
    let inp = Box::from_raw(inp);
    let ret = inp.0.argv.as_ref().map(|vec| vec.len()).unwrap_or(0);
    Box::into_raw(inp);
    ret
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_get_argv_item_len(inp: *mut B7Input, index: usize) -> usize {
    let inp = Box::from_raw(inp);
    let ret = inp.0.argv.as_ref().expect("argv is empty")[index].len();
    Box::into_raw(inp);
    ret
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_get_argv_item_byte(
    inp: *mut B7Input,
    arg_index: usize,
    byte_index: usize,
) -> u8 {
    let inp = Box::from_raw(inp);
    let ret = inp.0.argv.as_ref().expect("argv is empty")[arg_index][byte_index];
    Box::into_raw(inp);
    ret
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_get_stdinlen(inp: *mut B7Input) -> u32 {
    let inp = Box::from_raw(inp);
    let ret = inp.0.stdinlen.unwrap_or(0);
    Box::into_raw(inp);
    ret
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_get_stdin_len(inp: *mut B7Input) -> usize {
    let inp = Box::from_raw(inp);
    let ret = inp.0.stdin.as_ref().map(|vec| vec.len()).unwrap_or(0);
    Box::into_raw(inp);
    ret
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_get_stdin_byte(inp: *mut B7Input, index: usize) -> u8 {
    let inp = Box::from_raw(inp);
    let ret = inp.0.stdin.as_ref().expect("stdin is empty")[index];
    Box::into_raw(inp);
    ret
}

#[no_mangle]
pub unsafe extern "C" fn b7_input_get_mem() {
    unimplemented!()
}
