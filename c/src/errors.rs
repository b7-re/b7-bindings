use crate::B7Input;

pub struct B7Error(pub b7::errors::SolverError);

#[repr(C)]
pub struct B7Results {
    pub input: *mut B7Input,
    pub error: *mut B7Error,
}
