use crate::generators::B7Input;

use std::os::raw::c_void;
use std::time::Duration;

use b7::generators::GenItem;

pub struct B7Ui(pub Box<dyn b7::b7tui::Ui>);

#[repr(C)]
pub enum B7UiKind {
    Env,
    Tui,
}

impl B7UiKind {
    pub fn into_ui(self) -> B7Ui {
        match self {
            B7UiKind::Env => B7Ui(Box::new(b7::b7tui::Env::new())),
            B7UiKind::Tui => B7Ui(Box::new(b7::b7tui::Tui::default())),
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn b7_ui_from_kind(kind: B7UiKind) -> *mut B7Ui {
    Box::into_raw(Box::new(kind.into_ui()))
}

#[no_mangle]
pub unsafe extern "C" fn b7_ui_from_custom(custom: *mut B7UiCustom) -> *mut B7Ui {
    Box::into_raw(Box::new(B7Ui(Box::from_raw(custom))))
}

pub struct B7UiResults(Box<Vec<(i64, (GenItem, b7::generators::Input))>>);

#[no_mangle]
pub unsafe extern "C" fn b7_ui_results_len(results: *mut B7UiResults) -> usize {
    let results = Box::from_raw(results);
    let ret = results.0.len();
    Box::into_raw(results);
    ret
}

#[no_mangle]
pub unsafe extern "C" fn b7_ui_results_get_ins_count(
    results: *mut B7UiResults,
    index: usize,
) -> i64 {
    let results = Box::from_raw(results);
    let ret = results.0[index].0;
    Box::into_raw(results);
    ret
}

#[no_mangle]
pub unsafe extern "C" fn b7_ui_results_get_gen_item(
    results: *mut B7UiResults,
    index: usize,
) -> u32 {
    let results = Box::from_raw(results);
    let ret = (results.0[index].1).0;
    Box::into_raw(results);
    ret
}

#[no_mangle]
pub unsafe extern "C" fn b7_ui_results_get_input(
    results: *mut B7UiResults,
    index: usize,
) -> *mut B7Input {
    let results = Box::from_raw(results);
    let ret = (results.0[index].1).1.clone();
    let ret = B7Input(ret);
    let ret = Box::new(ret);
    let ret = Box::into_raw(ret);
    Box::into_raw(results);
    ret
}

pub type B7UiCustomFuncUpdateType = fn(*mut c_void, *mut B7UiResults, u64) -> bool;
pub type B7UiCustomFuncWaitType = fn(*mut c_void) -> bool;
pub type B7UiCustomFuncDoneType = fn(*mut c_void) -> bool;
pub type B7UiCustomFuncSetTimeoutType = fn(*mut c_void, u64);
pub type B7UiCustomFuncGetTimeoutType = fn(*mut c_void) -> u64;

pub struct B7UiCustom {
    data: *mut c_void,
    func_update: B7UiCustomFuncUpdateType,
    func_wait: B7UiCustomFuncWaitType,
    func_done: B7UiCustomFuncDoneType,
    func_set_timeout: B7UiCustomFuncSetTimeoutType,
    func_get_timeout: B7UiCustomFuncGetTimeoutType,
}

#[no_mangle]
pub unsafe extern "C" fn b7_ui_custom_new(
    data: *mut c_void,
    func_update: B7UiCustomFuncUpdateType,
    func_wait: B7UiCustomFuncWaitType,
    func_done: B7UiCustomFuncDoneType,
    func_set_timeout: B7UiCustomFuncSetTimeoutType,
    func_get_timeout: B7UiCustomFuncGetTimeoutType,
) -> *mut B7UiCustom {
    Box::into_raw(Box::new(B7UiCustom {
        data,
        func_update,
        func_wait,
        func_done,
        func_set_timeout,
        func_get_timeout,
    }))
}

impl b7::b7tui::Ui for B7UiCustom {
    fn update(
        &mut self,
        results: Box<Vec<(i64, (GenItem, b7::generators::Input))>>,
        min: u64,
    ) -> bool {
        let results = Box::into_raw(Box::new(B7UiResults(results)));
        (self.func_update)(self.data, results, min)
    }

    fn wait(&mut self) -> bool {
        (self.func_wait)(self.data)
    }

    fn done(&mut self) -> bool {
        (self.func_done)(self.data)
    }

    fn set_timeout(&mut self, timeout: Duration) {
        (self.func_set_timeout)(self.data, timeout.as_secs())
    }

    fn get_timeout(&mut self) -> Duration {
        Duration::from_secs((self.func_get_timeout)(self.data))
    }
}
