mod brute;
mod errors;
mod generators;
mod ui;

pub use brute::*;
pub use errors::*;
pub use generators::*;
pub use ui::*;

use std::ffi;
use std::os::raw::c_char;
use std::ptr;
use std::time::Duration;

pub struct B7Opts(b7::B7Opts);

#[no_mangle]
pub unsafe extern "C" fn b7_opts_new(path: *const c_char) -> *mut B7Opts {
    Box::into_raw(Box::new(B7Opts(b7::B7Opts::new(
        ffi::CStr::from_ptr(path).to_str().unwrap().to_string(),
    ))))
}

#[no_mangle]
pub unsafe extern "C" fn b7_opts_free(opts: *mut B7Opts) {
    Box::from_raw(opts);
}

#[no_mangle]
pub unsafe extern "C" fn b7_run(opts: *mut B7Opts) -> B7Results {
    let mut opts = Box::from_raw(opts);
    match opts.0.run() {
        Ok(input) => B7Results {
            input: Box::into_raw(Box::new(B7Input(input))),
            error: ptr::null_mut(),
        },
        Err(err) => B7Results {
            input: ptr::null_mut(),
            error: Box::into_raw(Box::new(B7Error(err))),
        },
    }
}

#[no_mangle]
pub unsafe extern "C" fn b7_opts_set_init_input(opts: *mut B7Opts, init_input: *mut B7Input) {
    let init_input = Box::from_raw(init_input).clone();
    let mut opts = Box::from_raw(opts);
    opts.0 = opts.0.init_input(init_input.0.clone());
    Box::into_raw(opts);
    Box::into_raw(init_input);
}

#[no_mangle]
pub unsafe extern "C" fn b7_opts_set_drop_ptrace(opts: *mut B7Opts, drop_ptrace: bool) {
    let mut opts = Box::from_raw(opts);
    opts.0 = opts.0.drop_ptrace(drop_ptrace);
    Box::into_raw(opts);
}

#[no_mangle]
pub unsafe extern "C" fn b7_opts_set_solve_argv(opts: *mut B7Opts, solve_argv: bool) {
    let mut opts = Box::from_raw(opts);
    opts.0 = opts.0.solve_argv(solve_argv);
    Box::into_raw(opts);
}

#[no_mangle]
pub unsafe extern "C" fn b7_opts_set_solve_stdin(opts: *mut B7Opts, solve_stdin: bool) {
    let mut opts = Box::from_raw(opts);
    opts.0 = opts.0.solve_stdin(solve_stdin);
    Box::into_raw(opts);
}

#[no_mangle]
pub unsafe extern "C" fn b7_opts_set_solver(opts: *mut B7Opts, solver: B7Solver) {
    let solver = B7Solver::to_rust(solver);
    let mut opts = Box::from_raw(opts);
    opts.0 = opts.0.solver(solver);
    Box::into_raw(opts);
}

#[no_mangle]
pub unsafe extern "C" fn b7_opts_set_ui(opts: *mut B7Opts, ui: *mut B7Ui) {
    let ui = Box::from_raw(ui);
    let mut opts = Box::from_raw(opts);
    opts.0 = opts.0.ui(ui.0);
    Box::into_raw(opts);
}

#[no_mangle]
pub unsafe extern "C" fn b7_opts_set_timeout(opts: *mut B7Opts, timeout: u64) {
    let timeout = Duration::from_secs(timeout);
    let mut opts = Box::from_raw(opts);
    opts.0 = opts.0.timeout(timeout);
    Box::into_raw(opts);
}
