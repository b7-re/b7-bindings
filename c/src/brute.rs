#[repr(C)]
pub enum B7Solver {
    Perf,
    #[cfg(feature = "dynamorio")]
    Dynamorio,
}

impl B7Solver {
    pub fn to_rust(self) -> Box<dyn b7::brute::InstCounter> {
        match self {
            B7Solver::Perf => Box::new(b7::perf::PerfSolver),
            #[cfg(feature = "dynamorio")]
            B7Solver::Dynamrio => Box::new(b7::dynamorio::DynamorioSolver),
        }
    }
}
