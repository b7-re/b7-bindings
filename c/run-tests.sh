#!/bin/sh

# Compile and run all the tests in tests/

set -e

make
git submodule init
git submodule update

for f in tests/*.c
do
    echo "$f"
    clang -Wall -Wextra -I . -L target/release -lb7 -o test-runner "$f"
    if ! LD_LIBRARY_PATH=target/release ./test-runner
    then
	echo "ERROR"
	exit 1
    fi
done
