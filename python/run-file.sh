#!/bin/sh

# Run a B7 python script with correct paths

set -e

make
git submodule init
git submodule update

LD_LIBRARY_PATH=../c/target/release PYTHONPATH=. "./$1"
