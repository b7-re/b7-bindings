%module b7
%rename("%(regex:/^[bB]7_*(.*)$/\\1/)s") ""; // Strip prefix from symbols
%{
#include "../c/b7.h"
%}
%typemap(in) bool (*)(void *, B7UiResults *, uint64_t) {
	$1 = (bool (*)(void *, B7UiResults *, uint64_t)) PyLong_AsVoidPtr($input);
}
%typemap(in) bool (*)(void *) {
	$1 = (bool (*)(void *)) PyLong_AsVoidPtr($input);
}
%typemap(in) void (*)(void *, uint64_t) {
	$1 = (void (*)(void *, uint64_t)) PyLong_AsVoidPtr($input);
}
%typemap(in) uint64_t (*)(void *) {
	$1 = (uint64_t (*)(void *)) PyLong_AsVoidPtr($input);
}
%typemap(in) B7UiResults * {
	$1 = (B7UiResults *) PyLong_AsVoidPtr($input);
}
%include "stdint.i"
%include "../c/b7.h"
%pythoncode
%{
import ctypes

UiCustomFuncUpdateType = ctypes.CFUNCTYPE(ctypes.c_bool, ctypes.c_void_p, ctypes.c_void_p, ctypes.c_uint64)
UiCustomFuncWaitType = ctypes.CFUNCTYPE(ctypes.c_bool, ctypes.c_void_p)
UiCustomFuncDoneType = ctypes.CFUNCTYPE(ctypes.c_bool, ctypes.c_void_p)
UiCustomFuncSetTimeoutType = ctypes.CFUNCTYPE(None, ctypes.c_void_p, ctypes.c_uint64)
UiCustomFuncGetTimeoutType = ctypes.CFUNCTYPE(ctypes.c_uint64, ctypes.c_void_p)

def ui_custom_new(data, func_update, func_wait, func_done, func_set_timeout,
		  func_get_timeout):

    func_update = ctypes.cast(func_update, ctypes.c_void_p).value
    func_wait = ctypes.cast(func_wait, ctypes.c_void_p).value
    func_done = ctypes.cast(func_done, ctypes.c_void_p).value
    func_set_timeout = ctypes.cast(func_set_timeout, ctypes.c_void_p).value
    func_get_timeout = ctypes.cast(func_get_timeout, ctypes.c_void_p).value

    return _b7.ui_custom_new(data, func_update, func_wait, func_done,
			     func_set_timeout, func_get_timeout)
%}
