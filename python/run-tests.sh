#!/bin/sh

# Run all the tests in tests/

set -e

make
git submodule init
git submodule update

for f in tests/*.py
do
    echo "$f"
    if ! ./run-file.sh "./$f"
    then
	echo "ERROR"
	exit 1
    fi
done
