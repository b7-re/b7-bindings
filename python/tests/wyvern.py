#!/usr/bin/env python3

import b7

flag = "dr4g0n_or_p4tric1an_it5_LLVM"

# Solve wyvern
opts = b7.opts_new("../test-bins/wyvern")
b7.opts_set_solve_stdin(opts, True)
b7.opts_set_timeout(opts, 5)
results = b7.run(opts)

# Assert results are sane
assert results.input is not None
assert results.error is None
inp = results.input
assert b7.input_get_stdin_len(inp) == 29
for (i, c) in enumerate(flag):
    assert ord(c) == b7.input_get_stdin_byte(inp, i)
