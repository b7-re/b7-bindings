#!/usr/bin/env python3

import ctypes

import b7

flag = "dr4g0n_or_p4tric1an_it5_LLVM"


@b7.UiCustomFuncUpdateType
def ui_update(data, results, minimum):
    print(data)
    print(results)
    print(minimum)
    return True


@b7.UiCustomFuncWaitType
def ui_wait(data):
    print(data)
    return True


@b7.UiCustomFuncDoneType
def ui_done(data):
    print(data)
    return True


@b7.UiCustomFuncSetTimeoutType
def ui_set_timeout(data, timeout):
    print(data)
    print(timeout)


@b7.UiCustomFuncGetTimeoutType
def ui_get_timeout(_data):
    return 5


# Create custom UI
custom = b7.ui_custom_new(None, ui_update, ui_wait, ui_done, ui_set_timeout,
                          ui_get_timeout)
ui = b7.ui_from_custom(custom)

# Solve wyvern
opts = b7.opts_new("../test-bins/wyvern")
b7.opts_set_solve_stdin(opts, True)
b7.opts_set_timeout(opts, 5)
b7.opts_set_ui(opts, ui)
results = b7.run(opts)

# Assert results are sane
assert results.input is not None
assert results.error is None
inp = results.input
assert b7.input_get_stdin_len(inp) == 29
for (i, c) in enumerate(flag):
    assert ord(c) == b7.input_get_stdin_byte(inp, i)
